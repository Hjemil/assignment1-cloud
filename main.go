package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var CLOCK = time.Now()

type Species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"bracketYear"`
}
type SKey struct {
	Species    string `json:"species"`
	SpeciesKey int    `json:speciesKey`
}

type Country struct {
	Code          string `json:"alpha2Code"`
	Countryname   string `json:"name"`
	Flag          string `json:"flag"`
	SpeciesResult []SKey `json:"results"`
}

type Diag struct {
	GBIF          string  `json:"GBIF"`
	Restcountries string  `json:"restcountries"`
	Version       string  `json:"version"`
	Uptime        float64 `json:"uptime"`
}

func GetRequest(Client *http.Client, URL string) (*http.Response, error) {
	request, err := http.NewRequest(http.MethodGet, URL, nil) //GET request
	var resp *http.Response
	if err == nil { //if there is no error, do the request
		resp, err = Client.Do(request)
	}

	return resp, err
}

func countries(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "Content-Type", "application/json")

	parts := strings.Split(r.URL.Path, "/") //Splitting up the url
	if len(parts) != 5 || parts[4] == "" {  //if there isn't 5 parts, or the last part is empty, send an error

		http.Error(w, "Expecting format .../country/--", http.StatusBadRequest)
		return
	}

	Client := http.DefaultClient
	URL := strings.Join([]string{"https://restcountries.eu/rest/v2/alpha/", parts[4]}, "") //Setting up the url to the database
	req, err := GetRequest(Client, URL)
	if err != nil { //if there is an error, send error message with status not found
		http.Error(w, "err.Error()", http.StatusNotFound)
		return
	}
	var body Country //Creating a Country struct variable that will get the decoded body from the database
	err = json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		http.Error(w, "err.Error()", http.StatusNotFound)
		return
	}

	limit := r.URL.Query().Get("limit") //Gets tthe limit value from the query in the url
	if limit == "" {                    //if there was no limit sendt, set it to 30
		limit = "30"
	}
	//Set the URL with country code and limit
	URL = strings.Join([]string{"http://api.gbif.org/v1/occurrence/search?country=", parts[4], "&limit=", limit}, "")
	req, err = GetRequest(Client, URL)

	err = json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(body) //Write the Country struct to website
}

func species(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "Content-Type", "application/json")

	parts := strings.Split(r.URL.Path, "/")
	if len(parts) != 5 && parts[4] != "" {
		status := http.StatusBadRequest
		http.Error(w, "Expecting format .../species/'speciesKey'", status)
		return
	}

	Client := http.DefaultClient
	URL := strings.Join([]string{"http://api.gbif.org/v1/species/", parts[4]}, "")

	req, err := GetRequest(Client, URL)
	var body Species

	err = json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	URL = strings.Join([]string{"http://api.gbif.org/v1/species/", parts[4], "/name"}, "")
	req, err = GetRequest(Client, URL)

	err = json.NewDecoder(req.Body).Decode(&body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	json.NewEncoder(w).Encode(body)
}

func diag(w http.ResponseWriter, r *http.Request) {
	http.Header.Add(w.Header(), "Content-Type", "application/json")

	var dDiag Diag
	req, err := GetRequest(http.DefaultClient, "http://api.gbif.org/v1/")
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	dDiag.GBIF = req.Status

	req, err = GetRequest(http.DefaultClient, "https://restcountries.eu/")
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	dDiag.Restcountries = req.Status

	dDiag.Version = "v1"

	dDiag.Uptime = time.Since(CLOCK).Seconds()

	json.NewEncoder(w).Encode(dDiag)
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/conservation/v1/country/", countries)
	http.HandleFunc("/conservation/v1/species/", species)
	http.HandleFunc("/conservation/v1/diag/", diag) // ensure to type complete URL when requesting
	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
